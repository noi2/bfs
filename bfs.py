from collections import deque


def bfs_search(graph_: dict, start, target):
    """Алгоритм поиска в ширину (BFS)

    Args:
        graph_: Граф
        start: Стартовая позиция (вершина)
        target: Цель

    Notes:
        Осуществляет поиск кратчайшего пути (если таковой имеется) от одной заданной вершины до другой
        Время выполнения алгоритма = O(V+E), где V-количество элементов (вершин), E-количество рёбер

    Returns:
        Булево значение найден путь до элемента или нет

    """
    print(f"Искомая вершина графа: {target}")
    print(f"Way: {start}", end=' -> ')  # Выводим начальную позицию

    search_queue = deque()  # Создаём очередь
    search_queue += graph_[start]  # Все соседи стартовой точки добавляются в очередь поиска
    searched = []  # Список уже проверенных элементов

    # Пока очередь не пуста
    while search_queue:
        element = search_queue.popleft()  # Из очереди извлекаем первый элемент

        if element not in searched:  # Проверяем, не был ли этот элемент уже в поиске
            print(element, end=' -> ')  # Выводим путь до элемента очереди

            if element == target:  # Если элемент является искомым
                return True
            else:
                search_queue += graph_[element]  # Соседи элемента добавляются в конец очереди
                searched.append(element)  # Заносим в список уже проверенных

    return False


if __name__ == '__main__':
    graph = {
        'Im': ['Ivan', 'Peter', 'Aleks', 'Mark', 'Liza'],

        # 1 уровень поиска
        'Ivan': ['Sam'],
        'Peter': ['Julian', 'Sam'],
        'Aleks': [],
        'Mark': ['John', 'Lida'],
        'Liza': ['Lida'],

        # 2 уровень поиска
        'Sam': [],
        'Julian': [],
        'John': [],
        'Lida': [],

        # No name
        'George': []
    }

    print("Caught" if bfs_search(graph_=graph, start="Im", target="Mark") else "No way")

    # print("Caught" if bfs_search(graph, "Im", "George") else "No way") # result: No Way
    # print("Caught" if bfs_search(graph, "Im", "Lida") else "No way") # result: Caught
